# Instructions

Challenges are written as failing unit tests.  To pass a challenge you must implement the corresponding function to make the test pass.  The code is structured using a test file and an implementation file.

e.g.

```
foo.js
foo.test.js
```

Within the implmentation file there will be one or more functions that are not fully implemented.  Provide an implementation and test it locally using `npm test`.

When you are finished, open a merge request to this repo.  The tests will be executed automatically and the success/failure will be visible in the merge request.  Once all tests pass you can close the merge request.
