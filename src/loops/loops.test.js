const loops = require("./loops");

describe("Loops", () => {
	test("Create a diamond", () => {
		let expected = "";
		expected += "\n    *    ";
		expected += "\n   ***   ";
		expected += "\n  *****  ";
		expected += "\n ******* ";
		expected += "\n*********";
		expected += "\n ******* ";
		expected += "\n  *****  ";
		expected += "\n   ***   ";
		expected += "\n    *    ";

		expect(loops.diamond()).toEqual(expected);
	});
});
