function diamond() {
	/**
	 * Use loops and other necessary logic to draw a diamond.  The challenge is intended to be
	 * solved character by character.  A simple solution using a single dimensional array
	 * preloaded with each line of the diamond doesn't count. :-)7
	 */
	return "";
};

module.exports = {
	diamond,
};
